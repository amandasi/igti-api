import express from 'express';
const app = express();
const port = 3000;

app.use((req, res, next) =>{
    console.log(new Date());
    next();
})

app.use(express.static('public'));
app.use('/images', express.static('public'));

app.get('/',(req,res) => res.send('Hello World Mandy!!!'));

app.listen(port,() =>{
    console.log(`App.listening on port ${port}`);
});